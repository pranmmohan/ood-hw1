package cs3500.hw01.duration;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/** Tests for the format method of {@link Duration}s.
*/
public abstract class AbstractDurationFormatTest {
  @Test
  public void formatExample1() {
    assertEquals("4 hours, 0 minutes, and 9 seconds",
                  hms(4, 0, 9)
                    .format("%h hours, %m minutes, and %s seconds"));
  }

  @Test
  public void formatExample2() {
    assertEquals("4:05:17",
                  hms(4, 5, 17).format("%h:%M:%S"));
  }

  // ADD MORE TESTS HERE
  // Your tests must only use hms(...) and sec(...) to construct new Durations
  // and must *not* directly say "new CompactDuration(...)" or
  // "new HmsDuration(...)"


  @Test
  public void formatExample3() {
    assertEquals("05:06:07",
        hms(5, 6, 7)
            .format("%H:%M:%S"));
    assertEquals("23:12:45",
        hms(23, 12, 45)
            .format("%H:%M:%S"));
  }


  /**
  Tests different valid ways of inserting multiple %s.
   */
  @Test
  public void formatPercents() {
    assertEquals("5i minutes",
                  hms(4, 5, 2)
                      .format("%mi minutes"));
    assertEquals("This is valid %x",
        hms(1,47,02)
            .format("This is valid %%x"));
    assertEquals("% x 4",
        hms(4, 3, 4).format("%% x %h"));
    assertEquals("%5",
        hms(5, 4, 5).format("%%%s"));
    assertEquals("5i minutes",
                  sec(14702)
                      .format("%mi minutes"));
    assertEquals("% x",
        sec(14702)
            .format("%% x"));
    assertEquals("% x 0",
        sec(45).format("%% x %h"));
    assertEquals("%5",
        sec(5).format("%%%s"));
    assertEquals("50%done",
        sec(50).format("%s%%done"));





  }

  /**
   * test all variants of %t.
   */

  @Test
  public void formatPercentT() {
    assertEquals("3600",
        sec(3600).format("%t"));
    assertEquals("3661",
        hms(1, 1, 1).format("%t"));
    assertEquals("65",
        hms(0, 1, 5).format("%t"));
    assertEquals("2",
        hms(0, 0, 2).format("%t"));
    assertEquals("3601",
        sec(3601).format("%t"));

  }

  /**
  Test all types of malformed templates.
   */

  @Test(expected = IllegalArgumentException.class)
  public void formatInvalidFormatter() {
    assertEquals(new IllegalArgumentException("Template is malformed"),
                  hms(4,4,2)
                    .format("%x"));
    assertEquals(new IllegalArgumentException("Template is malformed"),
                  hms(4,2,3)
                    .format("%%%%%%%"));
    assertEquals(new IllegalArgumentException("Template is malformed"),
                  hms(4,2,1)
                    .format("% x"));
    assertEquals(new IllegalArgumentException("Template is malformed"),
                  sec(14642)
                    .format("Not Valid%x"));
    assertEquals(new IllegalArgumentException("Template is malformed"),
                sec(14523)
                    .format("%%%%%%%"));
  }


  /**
   * tests missing formatters.
   */
  @Test(expected = IllegalArgumentException.class)
  public void formatMissingFormatter() {
    assertEquals(new IllegalArgumentException("Invalid Template"),
        sec(14523)
            .format("%     "));
    assertEquals(new IllegalArgumentException("Template is malformed"),
        sec(12323)
            .format("% h"));

  }

  /**
   * Test just has a formatter and nothing else.
   */
  @Test(expected = IllegalArgumentException.class)
  public void formatStringEndsOnPercent() {
    assertEquals(new IllegalArgumentException(),
        hms(3,67,78).format("%"));

  }


  /**
   * Test formatters %s and %S.
   */
  @Test
  public void formatSeconds() {
    assertEquals("5 hours, 4 seconds",
                  hms(5, 5,4)
                    .format("%h hours, %s seconds"));

    assertEquals("5 hours, 04 seconds",
                  hms(5, 5, 4)
                    .format("%h hours, %S seconds"));
    assertEquals("5 hours, 59 seconds",
                  hms(5, 5, 59)
                    .format("%h hours, %s seconds"));
    assertEquals("59 seconds",
                  hms(5, 5, 59)
                    .format("%S seconds"));

    assertEquals("5 hours, 4 seconds",
                  sec(18304)
                    .format("%h hours, %s seconds"));
    assertEquals("5 hours, 04 seconds",
                  sec(18304)
                    .format("%h hours, %S seconds"));
    assertEquals("5 hours, 59 seconds",
                  sec(18359)
                    .format("%h hours, %s seconds"));
  }


  /**
   Tests formatters %m and %M.
   */
  @Test
  public void formatMinutes() {
    assertEquals("5 hours, 8 minutes, 4 seconds",
                  hms(5, 8,4)
                    .format("%h hours, %m minutes, %s seconds"));
    assertEquals("5 hours, 08 minutes, 4 seconds",
                  hms(5, 8,4)
                    .format("%h hours, %M minutes, %s seconds"));
    assertEquals("5 hours, 48 minutes, 4 seconds",
                 hms(5, 48,4)
                    .format("%h hours, %m minutes, %s seconds"));
    assertEquals("48",
                 hms(5, 48, 2)
                     .format("%M"));


    assertEquals("5 hours, 8 minutes, 4 seconds",
                sec(18484)
                  .format("%h hours, %m minutes, %s seconds"));
    assertEquals("5 hours, 08 minutes, 4 seconds",
                sec(18484)
                  .format("%h hours, %M minutes, %s seconds"));
    assertEquals("5 hours, 48 minutes, 4 seconds",
                sec(20884)
                  .format("%h hours, %m minutes, %s seconds"));
  }

  /**
   * Tests formatters %H and %h.
   */
  @Test
  public void formatHours() {
    assertEquals("5 hours, 8 minutes, 4 seconds",
                  hms(5, 8,4)
                    .format("%h hours, %m minutes, %s seconds"));
    assertEquals("05 hours, 7 minutes, 4 seconds",
                hms(5, 7,4)
                    .format("%H hours, %m minutes, %s seconds"));
    assertEquals("154 hours, 58 minutes, 4 seconds",
                  hms(154, 58,4)
                    .format("%h hours, %m minutes, %s seconds"));
    assertEquals("14",
                  hms(14, 2, 3)
                      .format("%H"));


    assertEquals("5 hours, 8 minutes, 4 seconds",
                  sec(18484)
                    .format("%h hours, %m minutes, %s seconds"));
    assertEquals("05 hours, 8 minutes, 4 seconds",
                  sec(18484)
                    .format("%H hours, %m minutes, %s seconds"));

  }


  /**
  Test multiple formatters in one string.
   */
  @Test
  public void formatLongFormats() {
    assertEquals("16, xq~!@#$^&*() 16: 12, 12: 42, 42: %%h, %s",
                  hms(12,42,16)
                    .format("%s, xq~!@#$^&*() %S: %h, %H: %m, %M: %%%%h, %%s"));
    assertEquals("16, xq~!@#$^&*() 16: 12, 12: 42, 42: %%h, %s",
                  sec(45736)
                    .format("%s, xq~!@#$^&*() %S: %h, %H: %m, %M: %%%%h, %%s"));
    assertEquals("",
                  sec(2632).format(""));
    assertEquals("",
                  hms(3, 85, 45).format(""));
    assertEquals("xcx    ", hms(2,3,45)
        .format("xcx    "));
    assertEquals("Some stuff before the formatter 1",
        hms(1, 2, 3)
        .format("Some stuff before the formatter %h"));



  }



  /*
    Leave this section alone: It contains two abstract methods to
    create Durations, and concrete implementations of this testing class
    will supply particular implementations of Duration to be used within 
    your tests.
   */
  /**
   * Constructs an instance of the class under test representing the duration
   * given in hours, minutes, and seconds.
   *
   * @param hours the hours in the duration
   * @param minutes the minutes in the duration
   * @param seconds the seconds in the duration
   * @return an instance of the class under test
   */
  protected abstract Duration hms(int hours, int minutes, int seconds);

  /**
   * Constructs an instance of the class under test representing the duration
   * given in seconds.
   *
   * @param inSeconds the total seconds in the duration
   * @return an instance of the class under test
   */
  protected abstract Duration sec(long inSeconds);

  public static final class HmsDurationTest extends AbstractDurationFormatTest {
    @Override
    protected Duration hms(int hours, int minutes, int seconds) {
      return new HmsDuration(hours, minutes, seconds);
    }

    @Override
    protected Duration sec(long inSeconds) {
      return new HmsDuration(inSeconds);
    }
  }

  public static final class CompactDurationTest extends AbstractDurationFormatTest {
    @Override
    protected Duration hms(int hours, int minutes, int seconds) {
      return new CompactDuration(hours, minutes, seconds);
    }

    @Override
    protected Duration sec(long inSeconds) {
      return new CompactDuration(inSeconds);
    }
  }
}
