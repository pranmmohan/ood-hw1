package cs3500.hw01.publication;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class WebpageTest {


  Publication newyorker = new Webpage("The New Yorker",
      "https://www.newyorker.com", "September 10, 2017");


  @Test
  public void testCiteApa() {
    assertEquals("The New Yorker. Retrieved September 10, 2017, from "
        + "https://www.newyorker.com.", newyorker.citeApa());

  }

  @Test
  public void testCiteMla() {
    assertEquals("\"The New Yorker.\" Web. September 10, 2017 "
        + "<https://www.newyorker.com>.", newyorker.citeMla());
  }

}